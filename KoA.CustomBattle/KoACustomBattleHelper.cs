﻿using System;
using System.Collections.Generic;
using System.Linq;
using KoA.Core.Missions;
using KoA.CustomBattle.UI;
using KoA.CustomBattle.UI.Components;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.Localization;
using TaleWorlds.MountAndBlade;
using TaleWorlds.MountAndBlade.CustomBattle.CustomBattle;
using TaleWorlds.ObjectSystem;

namespace KoA.CustomBattle
{
    public static class KoACustomBattleHelper
    {
        public static void StartGame(KoACustomBattleData data)
        {
            Game.Current.PlayerTroop = data.PlayerCharacter;
            if (data.GameType == CustomBattleGameType.Siege)
                KoAMissions.OpenSiegeMissionWithDeployment(data.SceneId, data.PlayerCharacter,
                    data.PlayerParty, data.EnemyParty, data.IsPlayerGeneral, data.WallHitpointPercentages,
                    data.HasAnySiegeTower, data.AttackerMachines, data.DefenderMachines, data.IsPlayerAttacker,
                    data.SceneUpgradeLevel, data.SeasonId, data.IsSallyOut, data.IsReliefAttack, data.TimeOfDay);
            else
                KoAMissions.OpenCustomBattleMission(data.SceneId, data.PlayerCharacter, data.PlayerParty,
                    data.EnemyParty, data.IsPlayerGeneral, data.PlayerSideGeneralCharacter, data.SceneLevel,
                    data.SeasonId, data.TimeOfDay);
        }

        public static int[] GetTroopCounts(int armySize, CustomBattleCompositionData compositionData)
        {
            int[] source = new int[4];
            --armySize;
            source[1] = MathF.Round(compositionData.RangedPercentage * (float)armySize);
            source[2] = MathF.Round(compositionData.CavalryPercentage * (float)armySize);
            source[3] = MathF.Round(compositionData.RangedCavalryPercentage * (float)armySize);
            source[0] = armySize - ((IEnumerable<int>)source).Sum();
            return source;
        }

        public static float[] GetWallHitpointPercentages(int breachedWallCount)
        {
            float[] hitpointPercentages = new float[2];
            switch (breachedWallCount)
            {
                case 0:
                    hitpointPercentages[0] = 1f;
                    hitpointPercentages[1] = 1f;
                    break;
                case 1:
                    int index = MBRandom.RandomInt(2);
                    hitpointPercentages[index] = 0.0f;
                    hitpointPercentages[1 - index] = 1f;
                    break;
                default:
                    hitpointPercentages[0] = 0.0f;
                    hitpointPercentages[1] = 0.0f;
                    break;
            }

            return hitpointPercentages;
        }

        public static SiegeEngineType GetSiegeWeaponType(SiegeEngineType siegeWeaponType)
        {
            if (siegeWeaponType == DefaultSiegeEngineTypes.Ladder)
                return DefaultSiegeEngineTypes.Ladder;
            if (siegeWeaponType == DefaultSiegeEngineTypes.Ballista)
                return DefaultSiegeEngineTypes.Ballista;
            if (siegeWeaponType == DefaultSiegeEngineTypes.FireBallista)
                return DefaultSiegeEngineTypes.FireBallista;
            if (siegeWeaponType == DefaultSiegeEngineTypes.Ram ||
                siegeWeaponType == DefaultSiegeEngineTypes.ImprovedRam)
                return DefaultSiegeEngineTypes.Ram;
            if (siegeWeaponType == DefaultSiegeEngineTypes.SiegeTower)
                return DefaultSiegeEngineTypes.SiegeTower;
            if (siegeWeaponType == DefaultSiegeEngineTypes.Onager ||
                siegeWeaponType == DefaultSiegeEngineTypes.Catapult)
                return DefaultSiegeEngineTypes.Onager;
            if (siegeWeaponType == DefaultSiegeEngineTypes.FireOnager ||
                siegeWeaponType == DefaultSiegeEngineTypes.FireCatapult)
                return DefaultSiegeEngineTypes.FireOnager;
            return siegeWeaponType == DefaultSiegeEngineTypes.Trebuchet ||
                   siegeWeaponType == DefaultSiegeEngineTypes.Bricole
                ? DefaultSiegeEngineTypes.Trebuchet
                : siegeWeaponType;
        }

        public static KoACustomBattleData PrepareBattleData(
            BasicCharacterObject playerCharacter,
            BasicCharacterObject playerSideGeneralCharacter,
            CustomBattleCombatant playerParty,
            CustomBattleCombatant enemyParty,
            CustomBattlePlayerSide playerSide,
            CustomBattlePlayerType battlePlayerType,
            CustomBattleGameType gameType,
            string scene,
            string season,
            float timeOfDay,
            Dictionary<SiegeEngineType, int> attackerMachines,
            Dictionary<SiegeEngineType, int> defenderMachines,
            float[] wallHitPointsPercentages,
            int sceneLevel,
            bool isSallyOut)
        {
            bool flag1 = playerSide == CustomBattlePlayerSide.Attacker;
            bool flag2 = battlePlayerType == CustomBattlePlayerType.Commander;
            KoACustomBattleData customBattleData = new KoACustomBattleData();
            customBattleData.GameType = gameType;
            customBattleData.SceneId = scene;
            customBattleData.PlayerCharacter = playerCharacter;
            customBattleData.PlayerParty = playerParty;
            customBattleData.EnemyParty = enemyParty;
            customBattleData.IsPlayerGeneral = flag2;
            customBattleData.PlayerSideGeneralCharacter = playerSideGeneralCharacter;
            customBattleData.SeasonId = season;
            customBattleData.SceneLevel = "";
            customBattleData.TimeOfDay = timeOfDay;
            if (customBattleData.GameType == CustomBattleGameType.Siege)
            {
                customBattleData.AttackerMachines = attackerMachines;
                customBattleData.DefenderMachines = defenderMachines;
                customBattleData.WallHitpointPercentages = wallHitPointsPercentages;
                customBattleData.HasAnySiegeTower = attackerMachines.Any<KeyValuePair<SiegeEngineType, int>>(
                    (Func<KeyValuePair<SiegeEngineType, int>, bool>)(mm =>
                        mm.Key == DefaultSiegeEngineTypes.SiegeTower));
                customBattleData.IsPlayerAttacker = flag1;
                customBattleData.SceneUpgradeLevel = sceneLevel;
                customBattleData.IsSallyOut = isSallyOut;
                customBattleData.IsReliefAttack = false;
            }

            return customBattleData;
        }

        public static CustomBattleCombatant[] GetCustomBattleParties(
            BasicCharacterObject playerCharacter,
            BasicCharacterObject playerSideGeneralCharacter,
            BasicCharacterObject enemyCharacter,
            BasicCultureObject playerFaction,
            int[] playerNumbers,
            List<BasicCharacterObject>[] playerTroopSelections,
            BasicCultureObject enemyFaction,
            int[] enemyNumbers,
            List<BasicCharacterObject>[] enemyTroopSelections,
            bool isPlayerAttacker)
        {
            CustomBattleCombatant[] customBattleParties = new CustomBattleCombatant[2]
            {
                new CustomBattleCombatant(new TextObject("{=sSJSTe5p}Player Party"), playerFaction,
                    Banner.CreateRandomBanner()),
                new CustomBattleCombatant(new TextObject("{=0xC75dN6}Enemy Party"), enemyFaction,
                    Banner.CreateRandomBanner())
            };
            customBattleParties[0].Side = isPlayerAttacker ? BattleSideEnum.Attacker : BattleSideEnum.Defender;
            customBattleParties[0].AddCharacter(playerCharacter, 1);
            if (playerSideGeneralCharacter != null)
            {
                customBattleParties[0].AddCharacter(playerSideGeneralCharacter, 1);
                customBattleParties[0].SetGeneral(playerSideGeneralCharacter);
            }
            else
                customBattleParties[0].SetGeneral(playerCharacter);

            customBattleParties[1].Side = customBattleParties[0].Side.GetOppositeSide();
            customBattleParties[1].AddCharacter(enemyCharacter, 1);
            for (int index = 0; index < customBattleParties.Length; ++index)
                PopulateListsWithDefaults(ref customBattleParties[index],
                    index == 0 ? playerNumbers : enemyNumbers,
                    index == 0 ? playerTroopSelections : enemyTroopSelections);
            return customBattleParties;
        }

        private static void PopulateListsWithDefaults(
            ref CustomBattleCombatant customBattleParties,
            int[] numbers,
            List<BasicCharacterObject>[] troopList)
        {
            BasicCultureObject basicCulture = customBattleParties.BasicCulture;
            if (troopList == null)
                troopList = new List<BasicCharacterObject>[4]
                {
                    new List<BasicCharacterObject>(),
                    new List<BasicCharacterObject>(),
                    new List<BasicCharacterObject>(),
                    new List<BasicCharacterObject>()
                };
            if (troopList[0].Count == 0)
                troopList[0] = new List<BasicCharacterObject>()
                {
                    GetDefaultTroopOfFormationForFaction(basicCulture, FormationClass.Infantry)
                };
            if (troopList[1].Count == 0)
                troopList[1] = new List<BasicCharacterObject>()
                {
                    GetDefaultTroopOfFormationForFaction(basicCulture, FormationClass.Ranged)
                };
            if (troopList[2].Count == 0)
                troopList[2] = new List<BasicCharacterObject>()
                {
                    GetDefaultTroopOfFormationForFaction(basicCulture, FormationClass.Cavalry)
                };
            if (troopList[3].Count == 0)
                troopList[3] = new List<BasicCharacterObject>()
                {
                    GetDefaultTroopOfFormationForFaction(basicCulture, FormationClass.HorseArcher)
                };
            if (!troopList[3].Any<BasicCharacterObject>() || troopList[3]
                    .All<BasicCharacterObject>((Func<BasicCharacterObject, bool>)(troop => troop == null)))
            {
                numbers[2] += numbers[3] / 3;
                numbers[1] += numbers[3] / 3;
                numbers[0] += numbers[3] / 3;
                numbers[0] += numbers[3] - numbers[3] / 3 * 3;
                numbers[3] = 0;
            }

            for (int index1 = 0; index1 < 4; ++index1)
            {
                int count = troopList[index1].Count;
                int number1 = numbers[index1];
                if (number1 > 0)
                {
                    float num1 = (float)number1 / (float)count;
                    float num2 = 0.0f;
                    for (int index2 = 0; index2 < count; ++index2)
                    {
                        double f = (double)num1 + (double)num2;
                        int number2 = MathF.Floor((float)f);
                        num2 = (float)f - (float)number2;
                        customBattleParties.AddCharacter(troopList[index1][index2], number2);
                        numbers[index1] -= number2;
                        if (index2 == count - 1 && numbers[index1] > 0)
                        {
                            customBattleParties.AddCharacter(troopList[index1][index2], numbers[index1]);
                            numbers[index1] = 0;
                        }
                    }
                }
            }
        }

        public static BasicCharacterObject GetDefaultTroopOfFormationForFaction(
            BasicCultureObject culture,
            FormationClass formation)
        {
            // if (culture.StringId.ToLower() == "empire")
            // {
            //     switch (formation)
            //     {
            //         case FormationClass.Infantry:
            //             return GetTroopFromId("imperial_veteran_infantryman");
            //         case FormationClass.Ranged:
            //             return GetTroopFromId("imperial_archer");
            //         case FormationClass.Cavalry:
            //             return GetTroopFromId("imperial_heavy_horseman");
            //         case FormationClass.HorseArcher:
            //             return GetTroopFromId("bucellarii");
            //     }
            // }
            // else if (culture.StringId.ToLower() == "sturgia")
            // {
            //     switch (formation)
            //     {
            //         case FormationClass.Infantry:
            //             return GetTroopFromId("sturgian_spearman");
            //         case FormationClass.Ranged:
            //             return GetTroopFromId("sturgian_archer");
            //         case FormationClass.Cavalry:
            //             return GetTroopFromId("sturgian_hardened_brigand");
            //     }
            // }
            // else if (culture.StringId.ToLower() == "aserai")
            // {
            //     switch (formation)
            //     {
            //         case FormationClass.Infantry:
            //             return GetTroopFromId("aserai_infantry");
            //         case FormationClass.Ranged:
            //             return GetTroopFromId("aserai_archer");
            //         case FormationClass.Cavalry:
            //             return GetTroopFromId("aserai_mameluke_cavalry");
            //         case FormationClass.HorseArcher:
            //             return GetTroopFromId("aserai_faris");
            //     }
            // }
            // else if (culture.StringId.ToLower() == "vlandia")
            // {
            //     switch (formation)
            //     {
            //         case FormationClass.Infantry:
            //             return GetTroopFromId("vlandian_swordsman");
            //         case FormationClass.Ranged:
            //             return GetTroopFromId("vlandian_hardened_crossbowman");
            //         case FormationClass.Cavalry:
            //             return GetTroopFromId("vlandian_knight");
            //     }
            // }
            // else if (culture.StringId.ToLower() == "battania")
            // {
            //     switch (formation)
            //     {
            //         case FormationClass.Infantry:
            //             return GetTroopFromId("battanian_picked_warrior");
            //         case FormationClass.Ranged:
            //             return GetTroopFromId("battanian_hero");
            //         case FormationClass.Cavalry:
            //             return GetTroopFromId("battanian_scout");
            //     }
            // }
            // else if (culture.StringId.ToLower() == "khuzait")
            // {
            //     switch (formation)
            //     {
            //         case FormationClass.Infantry:
            //             return GetTroopFromId("khuzait_spear_infantry");
            //         case FormationClass.Ranged:
            //             return GetTroopFromId("khuzait_archer");
            //         case FormationClass.Cavalry:
            //             return GetTroopFromId("khuzait_lancer");
            //         case FormationClass.HorseArcher:
            //             return GetTroopFromId("khuzait_horse_archer");
            //     }
            // }

            return (BasicCharacterObject)null;
        }

        public static BasicCharacterObject GetBannermanTroopOfFormationForFaction(
            BasicCultureObject culture,
            FormationClass formation)
        {
            // if (culture.StringId.ToLower() == "empire")
            // {
            //     if (formation == FormationClass.Infantry)
            //         return GetTroopFromId("imperial_infantry_banner_bearer");
            //     if (formation == FormationClass.Cavalry)
            //         return GetTroopFromId("imperial_cavalry_banner_bearer");
            // }
            // else if (culture.StringId.ToLower() == "sturgia")
            // {
            //     if (formation == FormationClass.Infantry)
            //         return GetTroopFromId("sturgian_infantry_banner_bearer");
            //     if (formation == FormationClass.Cavalry)
            //         return GetTroopFromId("sturgian_cavalry_banner_bearer");
            // }
            // else if (culture.StringId.ToLower() == "aserai")
            // {
            //     if (formation == FormationClass.Infantry)
            //         return GetTroopFromId("aserai_infantry_banner_bearer");
            //     if (formation == FormationClass.Cavalry)
            //         return GetTroopFromId("aserai_cavalry_banner_bearer");
            // }
            // else if (culture.StringId.ToLower() == "vlandia")
            // {
            //     if (formation == FormationClass.Infantry)
            //         return GetTroopFromId("vlandian_infantry_banner_bearer");
            //     if (formation == FormationClass.Cavalry)
            //         return GetTroopFromId("vlandian_cavalry_banner_bearer");
            // }
            // else if (culture.StringId.ToLower() == "battania")
            // {
            //     if (formation == FormationClass.Infantry)
            //         return GetTroopFromId("battanian_woodrunner");
            //     if (formation == FormationClass.Cavalry)
            //         return GetTroopFromId("battanian_cavalry_banner_bearer");
            // }
            // else if (culture.StringId.ToLower() == "khuzait")
            // {
            //     if (formation == FormationClass.Infantry)
            //         return GetTroopFromId("khuzait_infantry_banner_bearer");
            //     if (formation == FormationClass.Cavalry)
            //         return GetTroopFromId("khuzait_cavalry_banner_bearer");
            // }

            return null;
        }

        private static BasicCharacterObject GetTroopFromId(string troopId) =>
            MBObjectManager.Instance.GetObject<BasicCharacterObject>(troopId);
    }
}