﻿using TaleWorlds.Core;
using TaleWorlds.Localization;
using TaleWorlds.MountAndBlade;

namespace KoA.CustomBattle
{
    public class SubModule : MBSubModuleBase
    {
        protected override void OnSubModuleLoad()
        {
            Module.CurrentModule.AddInitialStateOption(new InitialStateOption("KoACustomBattle",
                new TextObject("KoA Custom Battle"), 69, () => MBGameManager.StartNewGame(new KoACustomBattleManager()),
                () => (false, null)));
        }

        protected override void OnGameStart(Game game, IGameStarter gameStarterObject)
        {
        }

        protected override void OnBeforeInitialModuleScreenSetAsRoot()
        {
            base.OnBeforeInitialModuleScreenSetAsRoot();
        }
    }
}