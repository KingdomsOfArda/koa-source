﻿using System.Collections.Generic;
using System.Linq;
using KoA.CustomBattle.UI.Components;
using SandBox.ViewModelCollection.Input;
using TaleWorlds.Core;
using TaleWorlds.InputSystem;
using TaleWorlds.Library;
using TaleWorlds.Localization;
using TaleWorlds.MountAndBlade.CustomBattle.CustomBattle;

namespace KoA.CustomBattle.UI
{
    public class KoACustomBattleMenuVM : ViewModel
    {
        private readonly KoACustomBattleState _customBattleState;
        private MBBindingList<KoACustomBattleSiegeMachineVM> _attackerMeleeMachines;
        private MBBindingList<KoACustomBattleSiegeMachineVM> _attackerRangedMachines;
        private string _backButtonText;
        private InputKeyItemVM _cancelInputKey;
        private MBBindingList<KoACustomBattleSiegeMachineVM> _defenderMachines;
        private KoACustomBattleMenuSideVM _enemySide;
        private GameTypeSelectionGroupVM _gameTypeSelectionGroup;
        private bool _isAttackerCustomMachineSelectionEnabled;
        private bool _isDefenderCustomMachineSelectionEnabled;
        private KoAMapSelectionGroupVM _mapSelectionGroup;
        private KoACustomBattleMenuSideVM _playerSide;
        private string _randomizeButtonText;
        private InputKeyItemVM _randomizeInputKey;
        private string _startButtonText;
        private InputKeyItemVM _startInputKey;
        private string _titleText;
        private TroopTypeSelectionPopUpVM _troopTypeSelectionPopUp;

        public KoACustomBattleMenuVM(KoACustomBattleState battleState)
        {
            _customBattleState = battleState;
            IsAttackerCustomMachineSelectionEnabled = false;
            TroopTypeSelectionPopUp = new TroopTypeSelectionPopUpVM();
            PlayerSide =
                new KoACustomBattleMenuSideVM(new TextObject("{=BC7n6qxk}PLAYER"), true, TroopTypeSelectionPopUp);
            EnemySide = new KoACustomBattleMenuSideVM(new TextObject("{=35IHscBa}ENEMY"), false,
                TroopTypeSelectionPopUp);
            PlayerSide.OppositeSide = EnemySide;
            EnemySide.OppositeSide = PlayerSide;
            MapSelectionGroup = new KoAMapSelectionGroupVM();
            GameTypeSelectionGroup = new GameTypeSelectionGroupVM(OnPlayerTypeChange, OnGameTypeChange);
            AttackerMeleeMachines = new MBBindingList<KoACustomBattleSiegeMachineVM>();
            for (var index = 0; index < 3; ++index)
                AttackerMeleeMachines.Add(new KoACustomBattleSiegeMachineVM(null, OnMeleeMachineSelection));
            AttackerRangedMachines = new MBBindingList<KoACustomBattleSiegeMachineVM>();
            for (var index = 0; index < 4; ++index)
                AttackerRangedMachines.Add(new KoACustomBattleSiegeMachineVM(null, OnAttackerRangedMachineSelection));
            DefenderMachines = new MBBindingList<KoACustomBattleSiegeMachineVM>();
            for (var index = 0; index < 4; ++index)
                DefenderMachines.Add(new KoACustomBattleSiegeMachineVM(null, OnDefenderRangedMachineSelection));
            RefreshValues();
            SetDefaultSiegeMachines();
        }

        [DataSourceProperty]
        public TroopTypeSelectionPopUpVM TroopTypeSelectionPopUp
        {
            get => _troopTypeSelectionPopUp;
            set
            {
                if (value == _troopTypeSelectionPopUp)
                    return;
                _troopTypeSelectionPopUp = value;
                OnPropertyChangedWithValue(value, nameof(TroopTypeSelectionPopUp));
            }
        }

        [DataSourceProperty]
        public bool IsAttackerCustomMachineSelectionEnabled
        {
            get => _isAttackerCustomMachineSelectionEnabled;
            set
            {
                if (value == _isAttackerCustomMachineSelectionEnabled)
                    return;
                _isAttackerCustomMachineSelectionEnabled = value;
                OnPropertyChangedWithValue(value, nameof(IsAttackerCustomMachineSelectionEnabled));
            }
        }

        [DataSourceProperty]
        public bool IsDefenderCustomMachineSelectionEnabled
        {
            get => _isDefenderCustomMachineSelectionEnabled;
            set
            {
                if (value == _isDefenderCustomMachineSelectionEnabled)
                    return;
                _isDefenderCustomMachineSelectionEnabled = value;
                OnPropertyChangedWithValue(value, nameof(IsDefenderCustomMachineSelectionEnabled));
            }
        }

        [DataSourceProperty]
        public string RandomizeButtonText
        {
            get => _randomizeButtonText;
            set
            {
                if (!(value != _randomizeButtonText))
                    return;
                _randomizeButtonText = value;
                OnPropertyChangedWithValue(value, nameof(RandomizeButtonText));
            }
        }

        [DataSourceProperty]
        public string TitleText
        {
            get => _titleText;
            set
            {
                if (!(value != _titleText))
                    return;
                _titleText = value;
                OnPropertyChangedWithValue(value, nameof(TitleText));
            }
        }

        [DataSourceProperty]
        public string BackButtonText
        {
            get => _backButtonText;
            set
            {
                if (!(value != _backButtonText))
                    return;
                _backButtonText = value;
                OnPropertyChangedWithValue(value, nameof(BackButtonText));
            }
        }

        [DataSourceProperty]
        public string StartButtonText
        {
            get => _startButtonText;
            set
            {
                if (!(value != _startButtonText))
                    return;
                _startButtonText = value;
                OnPropertyChangedWithValue(value, nameof(StartButtonText));
            }
        }

        [DataSourceProperty]
        public KoACustomBattleMenuSideVM EnemySide
        {
            get => _enemySide;
            set
            {
                if (value == _enemySide)
                    return;
                _enemySide = value;
                OnPropertyChangedWithValue(value, nameof(EnemySide));
            }
        }

        [DataSourceProperty]
        public KoACustomBattleMenuSideVM PlayerSide
        {
            get => _playerSide;
            set
            {
                if (value == _playerSide)
                    return;
                _playerSide = value;
                OnPropertyChangedWithValue(value, nameof(PlayerSide));
            }
        }

        [DataSourceProperty]
        public GameTypeSelectionGroupVM GameTypeSelectionGroup
        {
            get => _gameTypeSelectionGroup;
            set
            {
                if (value == _gameTypeSelectionGroup)
                    return;
                _gameTypeSelectionGroup = value;
                OnPropertyChangedWithValue(value, nameof(GameTypeSelectionGroup));
            }
        }

        [DataSourceProperty]
        public KoAMapSelectionGroupVM MapSelectionGroup
        {
            get => _mapSelectionGroup;
            set
            {
                if (value == _mapSelectionGroup)
                    return;
                _mapSelectionGroup = value;
                OnPropertyChangedWithValue(value, nameof(MapSelectionGroup));
            }
        }

        [DataSourceProperty]
        public MBBindingList<KoACustomBattleSiegeMachineVM> AttackerMeleeMachines
        {
            get => _attackerMeleeMachines;
            set
            {
                if (value == _attackerMeleeMachines)
                    return;
                _attackerMeleeMachines = value;
                OnPropertyChangedWithValue(value, nameof(AttackerMeleeMachines));
            }
        }

        [DataSourceProperty]
        public MBBindingList<KoACustomBattleSiegeMachineVM> AttackerRangedMachines
        {
            get => _attackerRangedMachines;
            set
            {
                if (value == _attackerRangedMachines)
                    return;
                _attackerRangedMachines = value;
                OnPropertyChangedWithValue(value, nameof(AttackerRangedMachines));
            }
        }

        [DataSourceProperty]
        public MBBindingList<KoACustomBattleSiegeMachineVM> DefenderMachines
        {
            get => _defenderMachines;
            set
            {
                if (value == _defenderMachines)
                    return;
                _defenderMachines = value;
                OnPropertyChangedWithValue(value, nameof(DefenderMachines));
            }
        }

        public InputKeyItemVM StartInputKey
        {
            get => _startInputKey;
            set
            {
                if (value == _startInputKey)
                    return;
                _startInputKey = value;
                OnPropertyChangedWithValue(value, nameof(StartInputKey));
            }
        }

        public InputKeyItemVM CancelInputKey
        {
            get => _cancelInputKey;
            set
            {
                if (value == _cancelInputKey)
                    return;
                _cancelInputKey = value;
                OnPropertyChangedWithValue(value, nameof(CancelInputKey));
            }
        }

        public InputKeyItemVM RandomizeInputKey
        {
            get => _randomizeInputKey;
            set
            {
                if (value == _randomizeInputKey)
                    return;
                _randomizeInputKey = value;
                OnPropertyChangedWithValue(value, nameof(RandomizeInputKey));
            }
        }

        private static CustomBattleCompositionData GetBattleCompositionDataFromCompositionGroup(
            KoAArmyCompositionGroupVM compositionGroup)
        {
            return new CustomBattleCompositionData(compositionGroup.RangedInfantryComposition.CompositionValue / 100f,
                compositionGroup.MeleeCavalryComposition.CompositionValue / 100f,
                compositionGroup.RangedCavalryComposition.CompositionValue / 100f);
        }

        private static List<BasicCharacterObject>[] GetTroopSelections(
            KoAArmyCompositionGroupVM armyComposition)
        {
            return new List<BasicCharacterObject>[4]
            {
                armyComposition.MeleeInfantryComposition.TroopTypes.Where(x => x.IsSelected).Select(x => x.Character)
                    .ToList(),
                armyComposition.RangedInfantryComposition.TroopTypes.Where(x => x.IsSelected).Select(x => x.Character)
                    .ToList(),
                armyComposition.MeleeCavalryComposition.TroopTypes.Where(x => x.IsSelected).Select(x => x.Character)
                    .ToList(),
                armyComposition.RangedCavalryComposition.TroopTypes.Where(x => x.IsSelected).Select(x => x.Character)
                    .ToList()
            };
        }

        private static void FillSiegeMachineCounts(
            Dictionary<SiegeEngineType, int> machineCounts,
            MBBindingList<KoACustomBattleSiegeMachineVM> machines)
        {
            foreach (var machine in machines)
                if (machine.SiegeEngineType != null)
                {
                    var siegeWeaponType = CustomBattleHelper.GetSiegeWeaponType(machine.SiegeEngineType);
                    if (!machineCounts.ContainsKey(siegeWeaponType))
                        machineCounts.Add(siegeWeaponType, 0);
                    machineCounts[siegeWeaponType]++;
                }
        }

        private void SetDefaultSiegeMachines()
        {
            AttackerMeleeMachines[0].SetMachineType(DefaultSiegeEngineTypes.SiegeTower);
            AttackerMeleeMachines[1].SetMachineType(DefaultSiegeEngineTypes.Ram);
            AttackerMeleeMachines[2].SetMachineType(DefaultSiegeEngineTypes.SiegeTower);
            AttackerRangedMachines[0].SetMachineType(DefaultSiegeEngineTypes.Trebuchet);
            AttackerRangedMachines[1].SetMachineType(DefaultSiegeEngineTypes.Onager);
            AttackerRangedMachines[2].SetMachineType(DefaultSiegeEngineTypes.Onager);
            AttackerRangedMachines[3].SetMachineType(DefaultSiegeEngineTypes.FireBallista);
            DefenderMachines[0].SetMachineType(DefaultSiegeEngineTypes.FireCatapult);
            DefenderMachines[1].SetMachineType(DefaultSiegeEngineTypes.FireCatapult);
            DefenderMachines[2].SetMachineType(DefaultSiegeEngineTypes.Catapult);
            DefenderMachines[3].SetMachineType(DefaultSiegeEngineTypes.FireBallista);
        }

        public void SetActiveState(bool isActive)
        {
            if (isActive)
            {
                EnemySide.UpdateCharacterVisual();
                PlayerSide.UpdateCharacterVisual();
            }
            else
            {
                EnemySide.CurrentSelectedCharacter = null;
                PlayerSide.CurrentSelectedCharacter = null;
            }
        }

        private void OnPlayerTypeChange(CustomBattlePlayerType playerType)
        {
            PlayerSide.OnPlayerTypeChange(playerType);
        }

        private void OnGameTypeChange(CustomBattleGameType gameType)
        {
            MapSelectionGroup.OnGameTypeChange(gameType);
        }

        public override void RefreshValues()
        {
            base.RefreshValues();
            RandomizeButtonText = GameTexts.FindText("str_randomize").ToString();
            StartButtonText = GameTexts.FindText("str_start").ToString();
            BackButtonText = GameTexts.FindText("str_back").ToString();
            TitleText = GameTexts.FindText("str_custom_battle").ToString();
            EnemySide.RefreshValues();
            PlayerSide.RefreshValues();
            AttackerMeleeMachines.ApplyActionOnAllItems(x => x.RefreshValues());
            AttackerRangedMachines.ApplyActionOnAllItems(x => x.RefreshValues());
            DefenderMachines.ApplyActionOnAllItems(x => x.RefreshValues());
            MapSelectionGroup.RefreshValues();
            TroopTypeSelectionPopUp?.RefreshValues();
        }

        private void OnMeleeMachineSelection(KoACustomBattleSiegeMachineVM selectedSlot)
        {
            var inquiryElements = new List<InquiryElement>();
            inquiryElements.Add(new InquiryElement(null, GameTexts.FindText("str_empty").ToString(), null));
            foreach (var attackerMeleeMachine in KoACustomBattleData.GetAllAttackerMeleeMachines())
                inquiryElements.Add(
                    new InquiryElement(attackerMeleeMachine, attackerMeleeMachine.Name.ToString(), null));
            InformationManager.ShowMultiSelectionInquiry(new MultiSelectionInquiryData(
                new TextObject("{=MVOWsP48}Select a Melee Machine").ToString(), string.Empty, inquiryElements, false, 1,
                GameTexts.FindText("str_done").ToString(), "",
                selectedElements => selectedSlot.SetMachineType(selectedElements.First().Identifier as SiegeEngineType),
                null));
        }

        private void OnAttackerRangedMachineSelection(KoACustomBattleSiegeMachineVM selectedSlot)
        {
            var inquiryElements = new List<InquiryElement>();
            inquiryElements.Add(new InquiryElement(null, GameTexts.FindText("str_empty").ToString(), null));
            foreach (var attackerRangedMachine in KoACustomBattleData.GetAllAttackerRangedMachines())
                inquiryElements.Add(new InquiryElement(attackerRangedMachine, attackerRangedMachine.Name.ToString(),
                    null));
            InformationManager.ShowMultiSelectionInquiry(new MultiSelectionInquiryData(
                new TextObject("{=SLZzfNPr}Select a Ranged Machine").ToString(), string.Empty, inquiryElements, false,
                1, GameTexts.FindText("str_done").ToString(), "",
                selectedElements => selectedSlot.SetMachineType(selectedElements[0].Identifier as SiegeEngineType),
                null));
        }

        private void OnDefenderRangedMachineSelection(KoACustomBattleSiegeMachineVM selectedSlot)
        {
            var inquiryElements = new List<InquiryElement>();
            inquiryElements.Add(new InquiryElement(null, GameTexts.FindText("str_empty").ToString(), null));
            foreach (var defenderRangedMachine in KoACustomBattleData.GetAllDefenderRangedMachines())
                inquiryElements.Add(new InquiryElement(defenderRangedMachine, defenderRangedMachine.Name.ToString(),
                    null));
            InformationManager.ShowMultiSelectionInquiry(new MultiSelectionInquiryData(
                new TextObject("{=SLZzfNPr}Select a Ranged Machine").ToString(), string.Empty, inquiryElements, false,
                1, GameTexts.FindText("str_done").ToString(), "",
                selectedElements => selectedSlot.SetMachineType(selectedElements[0].Identifier as SiegeEngineType),
                null));
        }

        private void ExecuteRandomizeAttackerSiegeEngines()
        {
            var e = new List<SiegeEngineType>();
            e.AddRange(KoACustomBattleData.GetAllAttackerMeleeMachines());
            e.Add(null);
            foreach (var attackerMeleeMachine in
                     _attackerMeleeMachines)
                attackerMeleeMachine.SetMachineType(e.GetRandomElement());
            e.Clear();
            e.AddRange(KoACustomBattleData.GetAllAttackerRangedMachines());
            e.Add(null);
            foreach (var attackerRangedMachine in
                     _attackerRangedMachines)
                attackerRangedMachine.SetMachineType(e.GetRandomElement());
        }

        private void ExecuteRandomizeDefenderSiegeEngines()
        {
            var e = new List<SiegeEngineType>();
            e.AddRange(KoACustomBattleData.GetAllDefenderRangedMachines());
            e.Add(null);
            foreach (var defenderMachine in
                     _defenderMachines)
                defenderMachine.SetMachineType(e.GetRandomElement());
        }

        public void ExecuteBack()
        {
            Debug.Print("EXECUTE BACK - PRESSED", color: Debug.DebugColor.Green);
            Game.Current.GameStateManager.PopState();
        }

        private KoACustomBattleData PrepareBattleData()
        {
            var selectedCharacter1 = PlayerSide.SelectedCharacter;
            var selectedCharacter2 = EnemySide.SelectedCharacter;
            var armySize1 = PlayerSide.CompositionGroup.ArmySize;
            var armySize2 = EnemySide.CompositionGroup.ArmySize;
            var isPlayerAttacker = GameTypeSelectionGroup.SelectedPlayerSide == CustomBattlePlayerSide.Attacker;
            var num = GameTypeSelectionGroup.SelectedPlayerType == CustomBattlePlayerType.Commander ? 1 : 0;
            BasicCharacterObject playerSideGeneralCharacter = null;
            if (num == 0)
            {
                var list = KoACustomBattleData.Characters.ToList();
                list.Remove(selectedCharacter1);
                list.Remove(selectedCharacter2);
                playerSideGeneralCharacter = list.GetRandomElement();
                --armySize1;
            }

            var troopCounts1 = KoACustomBattleHelper.GetTroopCounts(armySize1,
                GetBattleCompositionDataFromCompositionGroup(PlayerSide.CompositionGroup));
            var compositionGroup =
                GetBattleCompositionDataFromCompositionGroup(EnemySide.CompositionGroup);
            var troopCounts2 = CustomBattleHelper.GetTroopCounts(armySize2, compositionGroup);
            var troopSelections1 =
                GetTroopSelections(PlayerSide.CompositionGroup);
            var troopSelections2 =
                GetTroopSelections(EnemySide.CompositionGroup);
            var faction1 = PlayerSide.FactionSelectionGroup.SelectedItem.Faction;
            var faction2 = EnemySide.FactionSelectionGroup.SelectedItem.Faction;
            var customBattleParties = KoACustomBattleHelper.GetCustomBattleParties(
                selectedCharacter1,
                playerSideGeneralCharacter, selectedCharacter2, faction1, troopCounts1, troopSelections1, faction2,
                troopCounts2, troopSelections2, isPlayerAttacker);
            Dictionary<SiegeEngineType, int> dictionary1 = null;
            Dictionary<SiegeEngineType, int> dictionary2 = null;
            float[] wallHitPointsPercentages = null;
            if (GameTypeSelectionGroup.SelectedGameType == CustomBattleGameType.Siege)
            {
                dictionary1 = new Dictionary<SiegeEngineType, int>();
                dictionary2 = new Dictionary<SiegeEngineType, int>();
                FillSiegeMachineCounts(dictionary1, _attackerMeleeMachines);
                FillSiegeMachineCounts(dictionary1, _attackerRangedMachines);
                FillSiegeMachineCounts(dictionary2, _defenderMachines);
                wallHitPointsPercentages =
                    KoACustomBattleHelper.GetWallHitpointPercentages(MapSelectionGroup.SelectedWallBreachedCount);
            }

            return KoACustomBattleHelper.PrepareBattleData(selectedCharacter1, playerSideGeneralCharacter,
                customBattleParties[0], customBattleParties[1], GameTypeSelectionGroup.SelectedPlayerSide,
                GameTypeSelectionGroup.SelectedPlayerType, GameTypeSelectionGroup.SelectedGameType,
                MapSelectionGroup.SelectedMap.MapId, MapSelectionGroup.SelectedSeasonId,
                MapSelectionGroup.SelectedTimeOfDay, dictionary1, dictionary2, wallHitPointsPercentages,
                MapSelectionGroup.SelectedSceneLevel, MapSelectionGroup.IsSallyOutSelected);
        }

        public void ExecuteStart()
        {
            KoACustomBattleHelper.StartGame(PrepareBattleData());
            Debug.Print("EXECUTE START - PRESSED", color: Debug.DebugColor.Green);
        }

        public void ExecuteRandomize()
        {
            GameTypeSelectionGroup.RandomizeAll();
            MapSelectionGroup.RandomizeAll();
            PlayerSide.Randomize();
            EnemySide.Randomize();
            ExecuteRandomizeAttackerSiegeEngines();
            ExecuteRandomizeDefenderSiegeEngines();
            Debug.Print("EXECUTE RANDOMIZE - PRESSED", color: Debug.DebugColor.Green);
        }

        private void ExecuteDoneDefenderCustomMachineSelection()
        {
            IsDefenderCustomMachineSelectionEnabled = false;
        }

        private void ExecuteDoneAttackerCustomMachineSelection()
        {
            IsAttackerCustomMachineSelectionEnabled = false;
        }

        public override void OnFinalize()
        {
            base.OnFinalize();
            StartInputKey.OnFinalize();
            CancelInputKey.OnFinalize();
            RandomizeInputKey.OnFinalize();
            TroopTypeSelectionPopUp?.OnFinalize();
        }

        public void SetStartInputKey(HotKey hotkey)
        {
            StartInputKey = InputKeyItemVM.CreateFromHotKey(hotkey, true);
        }

        public void SetCancelInputKey(HotKey hotkey)
        {
            CancelInputKey = InputKeyItemVM.CreateFromHotKey(hotkey, true);
            TroopTypeSelectionPopUp?.SetCancelInputKey(hotkey);
        }

        public void SetRandomizeInputKey(HotKey hotkey)
        {
            RandomizeInputKey = InputKeyItemVM.CreateFromHotKey(hotkey, true);
        }
    }
}