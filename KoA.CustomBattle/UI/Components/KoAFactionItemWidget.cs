﻿using TaleWorlds.GauntletUI;

namespace KoA.CustomBattle.UI.Components
{
    public class KoAFactionItemWidget : ButtonWidget
    {
        private string _baseBrushName = "MPLobby.ClassFilter.FactionButton";
        private string _culture;

        public KoAFactionItemWidget(UIContext context)
            : base(context)
        {
        }

        [Editor]
        public string BaseBrushName
        {
            get => _baseBrushName;
            set
            {
                if (!(value != _baseBrushName))
                    return;
                _baseBrushName = value;
                OnPropertyChanged(value, nameof(BaseBrushName));
                OnCultureChanged();
            }
        }

        [Editor]
        public string Culture
        {
            get => _culture;
            set
            {
                if (!(_culture != value))
                    return;
                _culture = value;
                OnPropertyChanged(value, nameof(Culture));
                OnCultureChanged();
            }
        }

        private void OnCultureChanged()
        {
            if (Culture == null)
                return;
            Brush = Context.BrushFactory.GetBrush(BaseBrushName + "." + "Vlandia");
        }
    }
}