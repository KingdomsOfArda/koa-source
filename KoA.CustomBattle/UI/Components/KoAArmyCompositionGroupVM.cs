﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade;
using TaleWorlds.MountAndBlade.CustomBattle.CustomBattle;

namespace KoA.CustomBattle.UI.Components
{
    public class KoAArmyCompositionGroupVM : ViewModel
    {
        private readonly List<BasicCharacterObject> _allCharacterObjects = new();

        private readonly MBReadOnlyList<SkillObject> _allSkills =
            Game.Current.ObjectManager.GetObjectTypeList<SkillObject>();

        private readonly bool _isPlayerSide;

        private int _armySize;
        private string _armySizeTitle;
        private int _maxArmySize;
        private KoAArmyCompositionItemVM _meleeCavalryComposition;
        private KoAArmyCompositionItemVM _meleeInfantryComposition;
        private int _minArmySize;
        private KoAArmyCompositionItemVM _rangedCavalryComposition;
        private KoAArmyCompositionItemVM _rangedInfantryComposition;
        private BasicCultureObject _selectedCulture;
        private TroopTypeSelectionPopUpVM _troopTypeSelectionPopUp;
        private bool _updatingSliders;
        public int[] CompositionValues;

        public KoAArmyCompositionGroupVM(
            bool isPlayerSide,
            TroopTypeSelectionPopUpVM troopTypeSelectionPopUp)
        {
            _isPlayerSide = isPlayerSide;
            MinArmySize = 1;
            MaxArmySize = (int)Math.Round(BannerlordConfig.GetRealBattleSize() / 2.0);
            foreach (var basicCharacterObject in Game.Current.ObjectManager.GetObjectTypeList<BasicCharacterObject>()
                .Where(c => c.IsSoldier))
                _allCharacterObjects.Add(basicCharacterObject);
            CompositionValues = new int[4];
            CompositionValues[0] = 25;
            CompositionValues[1] = 25;
            CompositionValues[2] = 25;
            CompositionValues[3] = 25;
            _troopTypeSelectionPopUp = troopTypeSelectionPopUp;
            MeleeInfantryComposition = new KoAArmyCompositionItemVM(
                KoAArmyCompositionItemVM.CompositionType.MeleeInfantry, _allCharacterObjects, _allSkills, UpdateSliders,
                troopTypeSelectionPopUp, CompositionValues);
            RangedInfantryComposition = new KoAArmyCompositionItemVM(
                KoAArmyCompositionItemVM.CompositionType.RangedInfantry, _allCharacterObjects, _allSkills,
                UpdateSliders, troopTypeSelectionPopUp, CompositionValues);
            MeleeCavalryComposition = new KoAArmyCompositionItemVM(
                KoAArmyCompositionItemVM.CompositionType.MeleeCavalry, _allCharacterObjects, _allSkills, UpdateSliders,
                troopTypeSelectionPopUp, CompositionValues);
            RangedCavalryComposition = new KoAArmyCompositionItemVM(
                KoAArmyCompositionItemVM.CompositionType.RangedCavalry, _allCharacterObjects, _allSkills, UpdateSliders,
                troopTypeSelectionPopUp, CompositionValues);
            _armySize = 1;
            RefreshValues();
        }

        [DataSourceProperty]
        public KoAArmyCompositionItemVM MeleeInfantryComposition
        {
            get => _meleeInfantryComposition;
            set
            {
                if (value == _meleeInfantryComposition)
                    return;
                _meleeInfantryComposition = value;
                OnPropertyChangedWithValue(value, nameof(MeleeInfantryComposition));
            }
        }

        [DataSourceProperty]
        public KoAArmyCompositionItemVM RangedInfantryComposition
        {
            get => _rangedInfantryComposition;
            set
            {
                if (value == _rangedInfantryComposition)
                    return;
                _rangedInfantryComposition = value;
                OnPropertyChangedWithValue(value, nameof(RangedInfantryComposition));
            }
        }

        [DataSourceProperty]
        public KoAArmyCompositionItemVM MeleeCavalryComposition
        {
            get => _meleeCavalryComposition;
            set
            {
                if (value == _meleeCavalryComposition)
                    return;
                _meleeCavalryComposition = value;
                OnPropertyChangedWithValue(value, nameof(MeleeCavalryComposition));
            }
        }

        [DataSourceProperty]
        public KoAArmyCompositionItemVM RangedCavalryComposition
        {
            get => _rangedCavalryComposition;
            set
            {
                if (value == _rangedCavalryComposition)
                    return;
                _rangedCavalryComposition = value;
                OnPropertyChangedWithValue(value, nameof(RangedCavalryComposition));
            }
        }

        [DataSourceProperty]
        public string ArmySizeTitle
        {
            get => _armySizeTitle;
            set
            {
                if (!(value != _armySizeTitle))
                    return;
                _armySizeTitle = value;
                OnPropertyChangedWithValue(value, nameof(ArmySizeTitle));
            }
        }

        public int ArmySize
        {
            get => _armySize;
            set
            {
                if (_armySize == (int)MathF.Clamp(value, MinArmySize, MaxArmySize))
                    return;
                _armySize = (int)MathF.Clamp(value, MinArmySize, MaxArmySize);
                OnPropertyChangedWithValue(value, nameof(ArmySize));
            }
        }

        public int MaxArmySize
        {
            get => _maxArmySize;
            set
            {
                if (_maxArmySize == value)
                    return;
                _maxArmySize = value;
                OnPropertyChangedWithValue(value, nameof(MaxArmySize));
            }
        }

        public int MinArmySize
        {
            get => _minArmySize;
            set
            {
                if (_minArmySize == value)
                    return;
                _minArmySize = value;
                OnPropertyChangedWithValue(value, nameof(MinArmySize));
            }
        }

        public override void RefreshValues()
        {
            base.RefreshValues();
            ArmySizeTitle = GameTexts.FindText("str_army_size").ToString();
            MeleeInfantryComposition.RefreshValues();
            RangedInfantryComposition.RefreshValues();
            MeleeCavalryComposition.RefreshValues();
            RangedCavalryComposition.RefreshValues();
        }

        private static int SumOfValues(int[] array, bool[] enabledArray, int excludedIndex = -1)
        {
            var num = 0;
            for (var index = 0; index < array.Length; ++index)
                if (enabledArray[index] && excludedIndex != index)
                    num += array[index];
            return num;
        }

        public void SetCurrentSelectedCulture(BasicCultureObject selectedCulture)
        {
            if (_selectedCulture == selectedCulture)
                return;
            MeleeInfantryComposition.SetCurrentSelectedCulture(selectedCulture);
            RangedInfantryComposition.SetCurrentSelectedCulture(selectedCulture);
            MeleeCavalryComposition.SetCurrentSelectedCulture(selectedCulture);
            RangedCavalryComposition.SetCurrentSelectedCulture(selectedCulture);
            _selectedCulture = selectedCulture;
        }

        private void UpdateSliders(int value, int changedSliderIndex)
        {
            if (_updatingSliders)
                return;
            _updatingSliders = true;
            var enabledArray = new bool[4]
            {
                !MeleeInfantryComposition.IsLocked,
                !RangedInfantryComposition.IsLocked,
                !MeleeCavalryComposition.IsLocked,
                !RangedCavalryComposition.IsLocked
            };
            var array = new int[4]
            {
                CompositionValues[0],
                CompositionValues[1],
                CompositionValues[2],
                CompositionValues[3]
            };
            var numArray = new int[4]
            {
                CompositionValues[0],
                CompositionValues[1],
                CompositionValues[2],
                CompositionValues[3]
            };
            var num1 = enabledArray.Count(s => s);
            if (enabledArray[changedSliderIndex])
                --num1;
            if (num1 > 0)
            {
                var num2 = SumOfValues(array, enabledArray);
                if (value >= num2)
                    value = num2;
                var num3 = value - array[changedSliderIndex];
                if (num3 != 0)
                {
                    var num4 = SumOfValues(array, enabledArray, changedSliderIndex);
                    var num5 = num4 - num3;
                    if (num5 > 0)
                    {
                        var num6 = 0;
                        numArray[changedSliderIndex] = value;
                        for (var index = 0; index < enabledArray.Length; ++index)
                            if (changedSliderIndex != index && enabledArray[index] && array[index] != 0)
                            {
                                var num7 = MathF.Round(array[index] / (float)num4 * num5);
                                num6 += num7;
                                numArray[index] = num7;
                            }

                        var num8 = num5 - num6;
                        if (num8 != 0)
                        {
                            var num7 = 0;
                            for (var index = 0; index < enabledArray.Length; ++index)
                                if (enabledArray[index] && index != changedSliderIndex && 0 < array[index] + num8 &&
                                    100 > array[index] + num8)
                                    ++num7;
                            for (var index = 0; index < enabledArray.Length; ++index)
                                if (enabledArray[index] && index != changedSliderIndex && 0 < array[index] + num8 &&
                                    100 > array[index] + num8)
                                {
                                    var num9 = (int)Math.Round(num8 / (double)num7);
                                    numArray[index] += num9;
                                    num8 -= num9;
                                }

                            if (num8 != 0)
                                for (var index = 0; index < enabledArray.Length; ++index)
                                    if (enabledArray[index] && index != changedSliderIndex &&
                                        0 <= array[index] + num8 && 100 >= array[index] + num8)
                                    {
                                        numArray[index] += num8;
                                        break;
                                    }
                        }
                    }
                    else
                    {
                        numArray[changedSliderIndex] = value;
                        for (var index = 0; index < enabledArray.Length; ++index)
                            if (changedSliderIndex != index && enabledArray[index])
                                numArray[index] = 0;
                    }
                }
            }

            SetArmyCompositionValue(0, numArray[0], MeleeInfantryComposition);
            SetArmyCompositionValue(1, numArray[1], RangedInfantryComposition);
            SetArmyCompositionValue(2, numArray[2], MeleeCavalryComposition);
            SetArmyCompositionValue(3, numArray[3], RangedCavalryComposition);
            _updatingSliders = false;
        }

        private void SetArmyCompositionValue(int index, int value, KoAArmyCompositionItemVM composition)
        {
            CompositionValues[index] = value;
            composition.RefreshCompositionValue();
        }

        public void ExecuteRandomize()
        {
            ArmySize = MBRandom.RandomInt(100);
            var num1 = MBRandom.RandomInt(100);
            var num2 = MBRandom.RandomInt(100);
            var num3 = MBRandom.RandomInt(100);
            var num4 = MBRandom.RandomInt(100);
            var num5 = num1 + num2 + num3 + num4;
            var compositionValue1 = (int)Math.Round(100.0 * (num1 / (double)num5));
            var compositionValue2 = (int)Math.Round(100.0 * (num2 / (double)num5));
            var compositionValue3 = (int)Math.Round(100.0 * (num3 / (double)num5));
            var compositionValue4 = 100 - (compositionValue1 + compositionValue2 + compositionValue3);
            MeleeInfantryComposition.ExecuteRandomize(compositionValue1);
            RangedInfantryComposition.ExecuteRandomize(compositionValue2);
            MeleeCavalryComposition.ExecuteRandomize(compositionValue3);
            RangedCavalryComposition.ExecuteRandomize(compositionValue4);
        }

        public void OnPlayerTypeChange(CustomBattlePlayerType playerType)
        {
            MinArmySize = playerType == CustomBattlePlayerType.Commander ? 1 : 2;
            ArmySize = _armySize;
        }
    }
}