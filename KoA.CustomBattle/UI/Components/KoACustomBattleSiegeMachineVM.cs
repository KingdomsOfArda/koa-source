﻿using System;
using TaleWorlds.Core;
using TaleWorlds.Library;

namespace KoA.CustomBattle.UI.Components
{
    public class KoACustomBattleSiegeMachineVM : ViewModel
    {
        private readonly Action<KoACustomBattleSiegeMachineVM> _onSelection;
        private bool _isRanged;
        private string _machineID;
        private string _name;

        public KoACustomBattleSiegeMachineVM(
            SiegeEngineType machineType,
            Action<KoACustomBattleSiegeMachineVM> onSelection)
        {
            _onSelection = onSelection;
            SetMachineType(machineType);
        }

        public SiegeEngineType SiegeEngineType { get; private set; }

        [DataSourceProperty]
        public bool IsRanged
        {
            get => _isRanged;
            set
            {
                if (value == _isRanged)
                    return;
                _isRanged = value;
                OnPropertyChangedWithValue(value, nameof(IsRanged));
            }
        }

        [DataSourceProperty]
        public string MachineID
        {
            get => _machineID;
            set
            {
                if (!(value != _machineID))
                    return;
                _machineID = value;
                OnPropertyChangedWithValue(value, nameof(MachineID));
            }
        }

        [DataSourceProperty]
        public string Name
        {
            get => _name;
            set
            {
                if (!(value != _name))
                    return;
                _name = value;
                OnPropertyChangedWithValue(value, nameof(Name));
            }
        }

        public void SetMachineType(SiegeEngineType machine)
        {
            SiegeEngineType = machine;
            Name = machine != null ? machine.StringId : "";
            IsRanged = machine != null && machine.IsRanged;
            MachineID = machine != null ? machine.StringId : "";
        }

        private void OnSelection()
        {
            _onSelection(this);
        }
    }
}