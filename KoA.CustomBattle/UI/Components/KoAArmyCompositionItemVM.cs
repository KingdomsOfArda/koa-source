﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaleWorlds.Core;
using TaleWorlds.Core.ViewModelCollection;
using TaleWorlds.Library;
using TaleWorlds.Localization;
using TaleWorlds.MountAndBlade.CustomBattle;
using TaleWorlds.MountAndBlade.CustomBattle.CustomBattle;

namespace KoA.CustomBattle.UI.Components
{
    public class KoAArmyCompositionItemVM : ViewModel
    {
        public enum CompositionType
        {
            MeleeInfantry,
            RangedInfantry,
            MeleeCavalry,
            RangedCavalry
        }

        private readonly List<BasicCharacterObject> _allCharacterObjects;
        private readonly MBReadOnlyList<SkillObject> _allSkills;
        private readonly int[] _compositionValues;
        private readonly Action<int, int> _onCompositionValueChanged;
        private readonly TroopTypeSelectionPopUpVM _troopTypeSelectionPopUp;
        private readonly CompositionType _type;
        private readonly StringItemWithHintVM _typeIconData;

        private HintViewModel _addTroopTypeHint;
        private BasicCultureObject _culture;
        private HintViewModel _invalidHint;
        private bool _isLocked;
        private bool _isValid;
        private MBBindingList<CustomBattleTroopTypeVM> _troopTypes;

        public KoAArmyCompositionItemVM(
            CompositionType type,
            List<BasicCharacterObject> allCharacterObjects,
            MBReadOnlyList<SkillObject> allSkills,
            Action<int, int> onCompositionValueChanged,
            TroopTypeSelectionPopUpVM troopTypeSelectionPopUp,
            int[] compositionValues)
        {
            _allCharacterObjects = allCharacterObjects;
            _allSkills = allSkills;
            _onCompositionValueChanged = onCompositionValueChanged;
            _troopTypeSelectionPopUp = troopTypeSelectionPopUp;
            _type = type;
            _compositionValues = compositionValues;
            _typeIconData = GetTroopTypeIconData(type);
            TroopTypes = new MBBindingList<CustomBattleTroopTypeVM>();
            InvalidHint = new HintViewModel(new TextObject("{=iSQTtNUD}This faction doesn't have this troop type."));
            AddTroopTypeHint = new HintViewModel(new TextObject("{=eMbuGGus}Select troops to spawn in formation."));
        }

        [DataSourceProperty]
        public MBBindingList<CustomBattleTroopTypeVM> TroopTypes
        {
            get => _troopTypes;
            set
            {
                if (value == _troopTypes)
                    return;
                _troopTypes = value;
                OnPropertyChangedWithValue(value, nameof(TroopTypes));
            }
        }

        [DataSourceProperty]
        public HintViewModel InvalidHint
        {
            get => _invalidHint;
            set
            {
                if (value == _invalidHint)
                    return;
                _invalidHint = value;
                OnPropertyChangedWithValue(value, nameof(InvalidHint));
            }
        }

        [DataSourceProperty]
        public HintViewModel AddTroopTypeHint
        {
            get => _addTroopTypeHint;
            set
            {
                if (value == _addTroopTypeHint)
                    return;
                _addTroopTypeHint = value;
                OnPropertyChangedWithValue(value, nameof(AddTroopTypeHint));
            }
        }

        [DataSourceProperty]
        public bool IsLocked
        {
            get => _isLocked;
            set
            {
                if (value == _isLocked)
                    return;
                _isLocked = value;
                OnPropertyChangedWithValue(value, nameof(IsLocked));
            }
        }

        [DataSourceProperty]
        public bool IsValid
        {
            get => _isValid;
            set
            {
                if (value == _isValid)
                    return;
                _isValid = value;
                OnPropertyChangedWithValue(value, nameof(IsValid));
                OnValidityChanged(value);
            }
        }

        [DataSourceProperty]
        public int CompositionValue
        {
            get => _compositionValues[(int)_type];
            set
            {
                if (value == _compositionValues[(int)_type])
                    return;
                _onCompositionValueChanged(value, (int)_type);
            }
        }

        public static StringItemWithHintVM GetTroopTypeIconData(
            CompositionType type,
            bool isBig = false)
        {
            var empty = TextObject.Empty;
            string str;
            TextObject text;
            switch (type)
            {
                case CompositionType.MeleeInfantry:
                    str = isBig ? "infantry_big" : "infantry";
                    text = GameTexts.FindText("str_troop_type_name", "Infantry");
                    break;
                case CompositionType.RangedInfantry:
                    str = isBig ? "bow_big" : "bow";
                    text = GameTexts.FindText("str_troop_type_name", "Ranged");
                    break;
                case CompositionType.MeleeCavalry:
                    str = isBig ? "cavalry_big" : "cavalry";
                    text = GameTexts.FindText("str_troop_type_name", "Cavalry");
                    break;
                case CompositionType.RangedCavalry:
                    str = isBig ? "horse_archer_big" : "horse_archer";
                    text = GameTexts.FindText("str_troop_type_name", "HorseArcher");
                    break;
                default:
                    return new StringItemWithHintVM("", TextObject.Empty);
            }

            return new StringItemWithHintVM("General\\TroopTypeIcons\\icon_troop_type_" + str,
                new TextObject("{=!}" + text));
        }

        public override void RefreshValues()
        {
            base.RefreshValues();
        }

        public void SetCurrentSelectedCulture(BasicCultureObject culture)
        {
            IsLocked = false;
            _culture = culture;
            PopulateTroopTypes();
        }

        public void ExecuteRandomize(int compositionValue)
        {
            IsValid = true;
            IsLocked = false;
            CompositionValue = compositionValue;
            IsValid = TroopTypes.Any();
            TroopTypes.ApplyActionOnAllItems(x => x.ExecuteRandomize());
            if (TroopTypes.Any(x => x.IsSelected) || !IsValid)
                return;
            TroopTypes.First().IsSelected = true;
        }

        public void ExecuteAddTroopTypes()
        {
            _troopTypeSelectionPopUp?.OpenPopUp(
                GameTexts.FindText("str_custom_battle_choose_troop", _type.ToString()).ToString(), TroopTypes);
        }

        public void RefreshCompositionValue()
        {
            OnPropertyChanged("CompositionValue");
        }

        private void OnValidityChanged(bool value)
        {
            IsLocked = false;
            if (!value)
                CompositionValue = 0;
            IsLocked = !value;
        }

        private void PopulateTroopTypes()
        {
            TroopTypes.Clear();
            var defaultCharacters = GetDefaultCharacters();
            foreach (var allCharacterObject in _allCharacterObjects)
                if (IsValidUnitItem(allCharacterObject))
                    TroopTypes.Add(new CustomBattleTroopTypeVM(allCharacterObject,
                        _troopTypeSelectionPopUp.OnItemSelectionToggled, _typeIconData, _allSkills,
                        defaultCharacters.Contains(allCharacterObject)));
            IsValid = true;
            IsLocked = false;
            if (TroopTypes.IsEmpty())
            {
                IsValid = false;
                CompositionValue = 0;
            }
            if (IsValid && !TroopTypes.Any(x => x.IsDefault))
                TroopTypes.First().IsDefault = true;
            TroopTypes.ApplyActionOnAllItems(x => x.IsSelected = true);
        }

        private bool IsValidUnitItem(BasicCharacterObject o)
        {
            if (o == null || _culture != o.Culture)
                return false;
            switch (_type)
            {
                case CompositionType.MeleeInfantry:
                    return o.DefaultFormationClass == FormationClass.Infantry ||
                           o.DefaultFormationClass == FormationClass.HeavyInfantry;
                case CompositionType.RangedInfantry:
                    return o.DefaultFormationClass == FormationClass.Ranged;
                case CompositionType.MeleeCavalry:
                    return o.DefaultFormationClass == FormationClass.Cavalry ||
                           o.DefaultFormationClass == FormationClass.HeavyCavalry ||
                           o.DefaultFormationClass == FormationClass.LightCavalry;
                case CompositionType.RangedCavalry:
                    return o.DefaultFormationClass == FormationClass.HorseArcher;
                default:
                    return false;
            }
        }

        private IReadOnlyList<BasicCharacterObject> GetDefaultCharacters()
        {
            var basicCharacterObjectList = new List<BasicCharacterObject>();
            var formation = FormationClass.NumberOfAllFormations;
            switch (_type)
            {
                case CompositionType.MeleeInfantry:
                    formation = FormationClass.Infantry;
                    break;
                case CompositionType.RangedInfantry:
                    formation = FormationClass.Ranged;
                    break;
                case CompositionType.MeleeCavalry:
                    formation = FormationClass.Cavalry;
                    break;
                case CompositionType.RangedCavalry:
                    formation = FormationClass.HorseArcher;
                    break;
            }

            basicCharacterObjectList.Add(
                KoACustomBattleState.Helper.GetDefaultTroopOfFormationForFaction(_culture, formation));
            return basicCharacterObjectList.AsReadOnly();
        }
    }
}