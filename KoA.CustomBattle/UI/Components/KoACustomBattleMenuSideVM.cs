﻿using TaleWorlds.Core;
using TaleWorlds.Core.ViewModelCollection;
using TaleWorlds.Library;
using TaleWorlds.Localization;
using TaleWorlds.MountAndBlade.CustomBattle.CustomBattle;
using TaleWorlds.MountAndBlade.CustomBattle.CustomBattle.SelectionItem;

namespace KoA.CustomBattle.UI.Components
{
    public class KoACustomBattleMenuSideVM : ViewModel
    {
        private readonly bool _isPlayerSide;
        private readonly TextObject _sideName;
        private MBBindingList<CharacterEquipmentItemVM> _armorsList;
        private SelectorVM<CharacterItemVM> _characterSelectionGroup;
        private KoAArmyCompositionGroupVM _compositionGroup;
        private CharacterViewModel _currentSelectedCharacter;
        private KoACustomBattleFactionSelectionVM _factionSelectionGroup;
        private string _factionText;
        private string _name;
        private string _titleText;
        private MBBindingList<CharacterEquipmentItemVM> _weaponsList;
        public KoACustomBattleMenuSideVM OppositeSide;

        public KoACustomBattleMenuSideVM(
            TextObject sideName,
            bool isPlayerSide,
            TroopTypeSelectionPopUpVM troopTypeSelectionPopUp)
        {
            _sideName = sideName;
            _isPlayerSide = isPlayerSide;
            CompositionGroup = new KoAArmyCompositionGroupVM(_isPlayerSide, troopTypeSelectionPopUp);
            FactionSelectionGroup = new KoACustomBattleFactionSelectionVM(CompositionGroup.SetCurrentSelectedCulture);
            CharacterSelectionGroup = new SelectorVM<CharacterItemVM>(0, OnCharacterSelection);
            ArmorsList = new MBBindingList<CharacterEquipmentItemVM>();
            WeaponsList = new MBBindingList<CharacterEquipmentItemVM>();
            RefreshValues();
        }

        public BasicCharacterObject SelectedCharacter { get; private set; }

        [DataSourceProperty]
        public CharacterViewModel CurrentSelectedCharacter
        {
            get => _currentSelectedCharacter;
            set
            {
                if (value == _currentSelectedCharacter)
                    return;
                _currentSelectedCharacter = value;
                OnPropertyChangedWithValue(value, nameof(CurrentSelectedCharacter));
            }
        }

        [DataSourceProperty]
        public MBBindingList<CharacterEquipmentItemVM> ArmorsList
        {
            get => _armorsList;
            set
            {
                if (value == _armorsList)
                    return;
                _armorsList = value;
                OnPropertyChangedWithValue(value, nameof(ArmorsList));
            }
        }

        [DataSourceProperty]
        public MBBindingList<CharacterEquipmentItemVM> WeaponsList
        {
            get => _weaponsList;
            set
            {
                if (value == _weaponsList)
                    return;
                _weaponsList = value;
                OnPropertyChangedWithValue(value, nameof(WeaponsList));
            }
        }

        [DataSourceProperty]
        public string FactionText
        {
            get => _factionText;
            set
            {
                if (!(value != _factionText))
                    return;
                _factionText = value;
                OnPropertyChangedWithValue(value, nameof(FactionText));
            }
        }

        [DataSourceProperty]
        public string TitleText
        {
            get => _titleText;
            set
            {
                if (!(value != _titleText))
                    return;
                _titleText = value;
                OnPropertyChangedWithValue(value, nameof(TitleText));
            }
        }

        [DataSourceProperty]
        public string Name
        {
            get => _name;
            set
            {
                if (!(value != _name))
                    return;
                _name = value;
                OnPropertyChangedWithValue(value, nameof(Name));
            }
        }

        [DataSourceProperty]
        public SelectorVM<CharacterItemVM> CharacterSelectionGroup
        {
            get => _characterSelectionGroup;
            set
            {
                if (value == _characterSelectionGroup)
                    return;
                _characterSelectionGroup = value;
                OnPropertyChangedWithValue(value, nameof(CharacterSelectionGroup));
            }
        }

        [DataSourceProperty]
        public KoAArmyCompositionGroupVM CompositionGroup
        {
            get => _compositionGroup;
            set
            {
                if (value == _compositionGroup)
                    return;
                _compositionGroup = value;
                OnPropertyChangedWithValue(value, nameof(CompositionGroup));
            }
        }

        [DataSourceProperty]
        public KoACustomBattleFactionSelectionVM FactionSelectionGroup
        {
            get => _factionSelectionGroup;
            set
            {
                if (value == _factionSelectionGroup)
                    return;
                _factionSelectionGroup = value;
                OnPropertyChangedWithValue(value, nameof(FactionSelectionGroup));
            }
        }

        public override void RefreshValues()
        {
            base.RefreshValues();
            Name = _sideName.ToString();
            FactionText = GameTexts.FindText("str_faction").ToString();
            TitleText = !_isPlayerSide
                ? new TextObject("{=QAYngoNQ}Enemy Character").ToString()
                : new TextObject("{=bLXleed8}Player Character").ToString();
            CharacterSelectionGroup.ItemList.Clear();
            foreach (var character in KoACustomBattleData.Characters)
                CharacterSelectionGroup.AddItem(new CharacterItemVM(character));
            CharacterSelectionGroup.SelectedIndex = _isPlayerSide ? 0 : 1;
            UpdateCharacterVisual();
            CompositionGroup.RefreshValues();
            CharacterSelectionGroup.RefreshValues();
            FactionSelectionGroup.RefreshValues();
        }

        public void OnPlayerTypeChange(CustomBattlePlayerType playerType)
        {
            CompositionGroup.OnPlayerTypeChange(playerType);
        }

        private void OnCharacterSelection(SelectorVM<CharacterItemVM> selector)
        {
            SelectedCharacter = selector.SelectedItem.Character;
            UpdateCharacterVisual();
            if (OppositeSide == null)
                return;
            var index = 0;
            foreach (var characterItemVm1 in selector.ItemList)
            {
                var characterItemVm2 = OppositeSide.CharacterSelectionGroup.ItemList[index];
                if (index == selector.SelectedIndex)
                    characterItemVm2.CanBeSelected = false;
                else
                    characterItemVm2.CanBeSelected = true;
                ++index;
            }
        }

        public void UpdateCharacterVisual()
        {
            CurrentSelectedCharacter = new CharacterViewModel(CharacterViewModel.StanceTypes.EmphasizeFace);
            CurrentSelectedCharacter.FillFrom(SelectedCharacter);
            ArmorsList.Clear();
            ArmorsList.Add(
                new CharacterEquipmentItemVM(SelectedCharacter.Equipment[EquipmentIndex.NumAllWeaponSlots].Item));
            ArmorsList.Add(new CharacterEquipmentItemVM(SelectedCharacter.Equipment[EquipmentIndex.Cape].Item));
            ArmorsList.Add(new CharacterEquipmentItemVM(SelectedCharacter.Equipment[EquipmentIndex.Body].Item));
            ArmorsList.Add(new CharacterEquipmentItemVM(SelectedCharacter.Equipment[EquipmentIndex.Gloves].Item));
            ArmorsList.Add(new CharacterEquipmentItemVM(SelectedCharacter.Equipment[EquipmentIndex.Leg].Item));
            WeaponsList.Clear();
            WeaponsList.Add(
                new CharacterEquipmentItemVM(SelectedCharacter.Equipment[EquipmentIndex.WeaponItemBeginSlot].Item));
            WeaponsList.Add(new CharacterEquipmentItemVM(SelectedCharacter.Equipment[EquipmentIndex.Weapon1].Item));
            WeaponsList.Add(new CharacterEquipmentItemVM(SelectedCharacter.Equipment[EquipmentIndex.Weapon2].Item));
            WeaponsList.Add(new CharacterEquipmentItemVM(SelectedCharacter.Equipment[EquipmentIndex.Weapon3].Item));
            WeaponsList.Add(new CharacterEquipmentItemVM(SelectedCharacter.Equipment[EquipmentIndex.Weapon4].Item));
        }

        public void Randomize()
        {
            CharacterSelectionGroup.ExecuteRandomize();
            FactionSelectionGroup.ExecuteRandomize();
            CompositionGroup.ExecuteRandomize();
        }
    }
}