﻿using System;
using KoA.Campaign.MiddleEarthCampaign.CharacterCreation;
using TaleWorlds.CampaignSystem;
using TaleWorlds.Core;
using TaleWorlds.Engine;
using TaleWorlds.MountAndBlade;
using TaleWorlds.SaveSystem.Load;

namespace KoA.Campaign.MiddleEarthCampaign
{
    public class KoAGameManager : MBGameManager
    {
        private readonly bool _loadingSavedGame;
        private readonly LoadResult _saveGameData;
        private readonly int _seed = 1234;

        public KoAGameManager()
        {
            _loadingSavedGame = false;
            _seed = (int)DateTime.Now.Ticks & ushort.MaxValue;
        }

        public KoAGameManager(int seed)
        {
            _loadingSavedGame = false;
            _seed = seed;
        }

        public KoAGameManager(LoadResult saveGameData)
        {
            _loadingSavedGame = true;
            _saveGameData = saveGameData;
        }

        public override void OnGameEnd(Game game)
        {
            MBDebug.SetErrorReportScene(null);
            base.OnGameEnd(game);
        }

        protected override void DoLoadingForGameManager(
            GameManagerLoadingSteps gameManagerLoadingStep,
            out GameManagerLoadingSteps nextStep)
        {
            nextStep = GameManagerLoadingSteps.None;
            switch (gameManagerLoadingStep)
            {
                case GameManagerLoadingSteps.PreInitializeZerothStep:
                    nextStep = GameManagerLoadingSteps.FirstInitializeFirstStep;
                    break;

                case GameManagerLoadingSteps.FirstInitializeFirstStep:
                    LoadModuleData(_loadingSavedGame);
                    nextStep = GameManagerLoadingSteps.WaitSecondStep;
                    break;

                case GameManagerLoadingSteps.WaitSecondStep:
                    if (!_loadingSavedGame)
                        StartNewGame();
                    nextStep = GameManagerLoadingSteps.SecondInitializeThirdState;
                    break;

                case GameManagerLoadingSteps.SecondInitializeThirdState:
                    MBGlobals.InitializeReferences();
                    if (!_loadingSavedGame)
                    {
                        MBDebug.Print("Initializing new KoA campaign...");
                        var campaign = new KoACampaign(CampaignGameMode.Campaign);
                        Game.CreateGame(campaign, this);
                        campaign.SetLoadingParameters(TaleWorlds.CampaignSystem.Campaign.GameLoadingType.NewCampaign,
                            _seed);
                        MBDebug.Print("Finished initializing new KoA campaign.");
                    }
                    else
                    {
                        MBDebug.Print("Loading KoA save...");
                        ((TaleWorlds.CampaignSystem.Campaign)Game.LoadSaveGame(_saveGameData, this).GameType)
                            .SetLoadingParameters(TaleWorlds.CampaignSystem.Campaign.GameLoadingType.SavedCampaign,
                                _seed);
                        MBDebug.Print("Finished loading KoA save.");
                    }

                    Game.Current.DoLoading();
                    nextStep = GameManagerLoadingSteps.PostInitializeFourthState;
                    break;

                case GameManagerLoadingSteps.PostInitializeFourthState:
                    var flag = true;
                    foreach (var subModule in Module.CurrentModule.SubModules)
                        flag = flag && subModule.DoLoading(Game.Current);

                    nextStep = flag
                        ? GameManagerLoadingSteps.FinishLoadingFifthStep
                        : GameManagerLoadingSteps.PostInitializeFourthState;
                    break;

                case GameManagerLoadingSteps.FinishLoadingFifthStep:
                    nextStep = Game.Current.DoLoading()
                        ? GameManagerLoadingSteps.None
                        : GameManagerLoadingSteps.FinishLoadingFifthStep;
                    break;
            }
        }

        public override void OnLoadFinished()
        {
            base.OnLoadFinished();
            if (!_loadingSavedGame)
            {
                LaunchKoACharacterCreation();
            }
            else
            {
                if (CampaignSiegeTestStatic.IsSiegeTestBuild)
                    CampaignSiegeTestStatic.DisableSiegeTest();
                Game.Current.GameStateManager.OnSavedGameLoadFinished();
                Game.Current.GameStateManager.CleanAndPushState(Game.Current.GameStateManager.CreateState<MapState>());
                if (Game.Current.GameStateManager.ActiveState is MapState activeState)
                    activeState.OnLoadingFinished();
                PartyBase.MainParty.Visuals.SetMapIconAsDirty();
                TaleWorlds.CampaignSystem.Campaign.Current.CampaignInformationManager.OnGameLoaded();
                foreach (var settlement in Settlement.All)
                    settlement.Party.Visuals.RefreshLevelMask(settlement.Party);
                CampaignEventDispatcher.Instance.OnGameLoadFinished();
            }
        }

        private void LaunchKoACharacterCreation()
        {
            Game.Current.GameStateManager.CleanAndPushState(
                Game.Current.GameStateManager.CreateState<KoACharacterCreationState>(
                    new KoACharacterCreationContentBase()
                )
            );
        }
    }
}