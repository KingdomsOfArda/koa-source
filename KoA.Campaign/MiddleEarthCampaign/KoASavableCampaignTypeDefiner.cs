﻿using TaleWorlds.SaveSystem;

namespace KoA.Campaign.MiddleEarthCampaign
{
    public class KoASavableCampaignTypeDefiner : SaveableTypeDefiner
    {
        public KoASavableCampaignTypeDefiner() : base(69420000)
        {
        }

        protected override void DefineClassTypes()
        {
            AddClassDefinition(typeof(KoACampaign), 1);
            //this.AddClassDefinition(typeof(MyCustomStoryMode), 2);
        }
    }
}