using TaleWorlds.CampaignSystem;
using TaleWorlds.CampaignSystem.SandBox.GameComponents;

namespace KoA.Campaign.Models
{
    public class KoAClanTierModel : DefaultClanTierModel
    {
        public override int GetCompanionLimit(Clan clan)
        {
            return 999;
        }
    }
}