﻿using HarmonyLib;
using SandBox;
using TaleWorlds.Engine;
using TaleWorlds.Library;
using TaleWorlds.MountAndBlade;

namespace KoA.Campaign.Patches
{
    [HarmonyPatch]
    public static class MapPatches
    {
        [HarmonyPrefix]
        [HarmonyPatch(typeof(MapScene), "Load")]
        public static bool CustomMapSceneLoad(MapScene __instance, ref Scene ____scene,
            ref MBAgentRendererSceneController ____agentRendererSceneController)
        {
            Debug.Print("Creating map scene");
            ____scene = Scene.CreateNewScene(false);
            ____scene.SetName("MapScene");
            ____scene.SetClothSimulationState(true);
            ____agentRendererSceneController =
                MBAgentRendererSceneController.CreateNewAgentRendererSceneController(____scene, 4096);
            ____scene.SetOcclusionMode(true);
            var initData = new SceneInitializationData(true);
            initData.UsePhysicsMaterials = true;
            initData.EnableFloraPhysics = false;
            initData.UseTerrainMeshBlending = false;
            Debug.Print("reading map scene");
            ____scene.Read("middle_earth", ref initData);
            Utilities.SetAllocationAlwaysValidScene(____scene);
            ____scene.DisableStaticShadows(true);
            ____scene.InvalidateTerrainPhysicsMaterials();
            MBMapScene.LoadAtmosphereData(____scene);
            __instance.DisableUnwalkableNavigationMeshes();
            MBMapScene.ValidateTerrainSoundIds();
            Debug.Print("Ticking map scene for first initialization");
            ____scene.Tick(0.1f);
            return false;
        }

        [HarmonyPatch(typeof(TaleWorlds.CampaignSystem.Campaign))]
        [HarmonyPatch("DefaultStartingPosition", MethodType.Getter)]
        public static class StartingPositionPatch
        {
            private static bool Prefix(ref Vec2 __result)
            {
                __result = new Vec2(1041.952f, 743.292f);
                return false;
            }
        }
    }
}