﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using KoA.Core.Util;
using KoA.Domain.Helpers;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.ObjectSystem;

namespace KoA.Core
{
    public class WeaponOverhaul
    {
        private readonly Dictionary<string, List<WeaponData>> _weaponDictionary =
            new();

        public void UpdateWeapons()
        {
            foreach (var keyValuePair in _weaponDictionary)
            {
                var obj = MBObjectManager.Instance.GetObject<ItemObject>(keyValuePair.Key);
                if (obj == null)
                {
                    InformationManager.DisplayMessage(new InformationMessage(
                        $"ItemObject '{keyValuePair.Key}' does not exist!",
                        new Color(1, 0, 0)));
                    continue;
                }

                var weaponDataList = keyValuePair.Value;
                if (weaponDataList.Count > obj.Weapons.Count)
                    throw new XmlException($"Too many Weapon Alternatives are given for: {keyValuePair.Key}");

                var weaponComponentData = obj.PrimaryWeapon;
                var weaponData = weaponDataList[0];
                Init(weaponComponentData, weaponData);

                for (var i = 1; i < weaponDataList.Count; i++) Init(obj.Weapons[i], weaponDataList[i]);
            }
        }

        private void Init(WeaponComponentData weaponComponentData, WeaponData weaponData)
        {
            weaponComponentData.Init(weaponComponentData.WeaponDescriptionId,
                weaponComponentData.PhysicsMaterial,
                weaponComponentData.ItemUsage,
                weaponData.ThrustDamageType != DamageTypes.Invalid
                    ? weaponData.ThrustDamageType
                    : weaponComponentData.ThrustDamageType,
                weaponData.SwingDamageType != DamageTypes.Invalid
                    ? weaponData.SwingDamageType
                    : weaponComponentData.SwingDamageType,
                weaponData.BodyArmor ?? weaponComponentData.BodyArmor,
                weaponData.WeaponLength ?? weaponComponentData.WeaponLength,
                weaponData.WeaponBalance ?? weaponComponentData.WeaponBalance,
                weaponData.Inertia ?? weaponComponentData.Inertia,
                weaponData.CenterOfMass ?? weaponComponentData.CenterOfMass,
                weaponData.Handling ?? weaponComponentData.Handling,
                weaponData.SwingDamageFactor ?? weaponComponentData.SwingDamageFactor,
                weaponData.ThrustDamageFactor ?? weaponComponentData.ThrustDamageFactor,
                weaponComponentData.MaxDataValue,
                weaponData.PassBySoundCode ?? weaponComponentData.PassbySoundCode,
                weaponData.Accuracy ?? weaponComponentData.Accuracy,
                weaponData.MissileSpeed ?? weaponComponentData.MissileSpeed,
                weaponComponentData.StickingFrame,
                weaponComponentData.AmmoClass,
                weaponData.SweetSpotReach ?? weaponComponentData.SweetSpotReach,
                weaponData.SwingSpeed ?? weaponComponentData.SwingSpeed,
                weaponData.SwingDamage ?? weaponComponentData.SwingDamage,
                weaponData.ThrustSpeed ?? weaponComponentData.ThrustSpeed,
                weaponData.ThrustDamage ?? weaponComponentData.ThrustDamage,
                weaponData.RotationSpeed ?? weaponComponentData.RotationSpeed,
                weaponData.WeaponTiers ?? weaponComponentData.WeaponTier,
                weaponData.ReloadPhaseCount ?? weaponComponentData.ReloadPhaseCount);
        }

        public void Load()
        {
            _weaponDictionary.Clear();
            foreach (var filePath in Directory.GetFiles(
                BasePath.Name + "Modules/KingdomsOfArda/ModuleData/items",
                "*.xml"))
                LoadWeaponData(filePath);
        }

        private void LoadWeaponData(string filePath)
        {
            XElement xItems = XmlManager.LoadXDocument(filePath).Element("Items");
            if (xItems == null)
            {
                return;
            }

            if (xItems.Attribute("id")?.Value != "Overhaul")
            {
                return;
            }

            XName qualifiedName = XName.Get("CraftedItem");
            XName piecesName = XName.Get("Pieces");

            foreach (var craftedItem in xItems.Descendants(qualifiedName)
                .Where(item => item.Elements(piecesName).Count() == 1))
            {
                var craftedItemId = craftedItem.Attribute("id")?.Value;
                if (string.IsNullOrEmpty(craftedItemId))
                    throw new XmlException("attribute: 'id' is expected on " + craftedItem.Value);

                var weaponDataList = new List<WeaponData> { GetWeaponDataOfElement(craftedItem) };
                weaponDataList.AddRange(craftedItem.Descendants("SecondaryWeapon")
                    .Select(secondaryWeapon => GetWeaponDataOfElement(secondaryWeapon))); 
                _weaponDictionary.Add(craftedItemId, weaponDataList);
            }
        }

        private WeaponData GetWeaponDataOfElement(XElement element)
        {
            var swingSpeed = element.Attribute("swing_speed")?.Value;
            var thrustSpeed = element.Attribute("thrust_speed")?.Value;
            var swingType = element.Attribute("swing_type")?.Value;
            var thrustType = element.Attribute("thrust_type")?.Value;
            var swingDamage = element.Attribute("swing_damage")?.Value;
            var thrustDamage = element.Attribute("thrust_damage")?.Value;
            var bodyArmor = element.Attribute("body_armor")?.Value;
            var weaponLength = element.Attribute("weapon_length")?.Value;
            var weaponBalance = element.Attribute("weapon_balance")?.Value;
            var inertia = element.Attribute("inertia")?.Value;
            var centerOfMass = element.Attribute("center_of_mass")?.Value;
            var swingDamageFactor = element.Attribute("swing_damage_factor")?.Value;
            var thrustDamageFactor = element.Attribute("thrust_damage_factor")?.Value;
            var passBySoundCode = element.Attribute("passBySoundCode")?.Value;
            var accuracy = element.Attribute("accuracy")?.Value;
            var missileSpeed = element.Attribute("missile_speed")?.Value;
            var sweetSpotReach = element.Attribute("sweet_spot_reach")?.Value;
            var rotationSpeed = element.Attribute("rotation_speed")?.Value;
            var handling = element.Attribute("handling")?.Value;
            var tier = element.Attribute("weapon_tier")?.Value;

            return new WeaponData
            {
                SwingSpeed = swingSpeed.AsNullableInt(),
                ThrustSpeed = thrustSpeed.AsNullableInt(),
                SwingDamageType = swingType.AsNullableEnum(DamageTypes.Invalid),
                ThrustDamageType = thrustType.AsNullableEnum(DamageTypes.Invalid),
                BodyArmor = bodyArmor.AsNullableInt(),
                WeaponLength = weaponLength.AsNullableInt(),
                WeaponBalance = weaponBalance.AsNullableInt(),
                Inertia = inertia.AsNullableFloat(),
                CenterOfMass = centerOfMass.AsNullableFloat(),
                Handling = handling.AsNullableInt(),
                SwingDamageFactor = swingDamageFactor.AsNullableFloat(),
                ThrustDamageFactor = thrustDamageFactor.AsNullableFloat(),
                PassBySoundCode = passBySoundCode,
                Accuracy = accuracy.AsNullableInt(),
                MissileSpeed = missileSpeed.AsNullableInt(),
                SweetSpotReach = sweetSpotReach.AsNullableFloat(),
                SwingDamage = swingDamage.AsNullableInt(),
                ThrustDamage = thrustDamage.AsNullableInt(),
                RotationSpeed = rotationSpeed.AsNullableVec3(),
                WeaponTiers = tier.AsNullableEnum<WeaponComponentData.WeaponTiers>()
            };
        }

        private struct WeaponData
        {
            public int? SwingSpeed { get; set; }
            public int? ThrustSpeed { get; set; }
            public DamageTypes SwingDamageType { get; set; }
            public DamageTypes ThrustDamageType { get; set; }
            public int? BodyArmor { get; set; }
            public int? WeaponLength { get; set; }
            public int? WeaponBalance { get; set; }
            public float? Inertia { get; set; }
            public float? CenterOfMass { get; set; }
            public int? Handling { get; set; }
            public float? SwingDamageFactor { get; set; }
            public float? ThrustDamageFactor { get; set; }
            public string PassBySoundCode { get; set; }
            public int? Accuracy { get; set; }
            public int? MissileSpeed { get; set; }
            public float? SweetSpotReach { get; set; }
            public int? SwingDamage { get; set; }
            public int? ThrustDamage { get; set; }
            public Vec3? RotationSpeed { get; set; }
            public WeaponComponentData.WeaponTiers? WeaponTiers { get; set; }
            public short? ReloadPhaseCount { get; set; }
        }
    }
}