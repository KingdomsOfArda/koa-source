﻿using System;
using System.Collections.Generic;
using System.Linq;
using HarmonyLib;
using KoA.Core.Missions.Behaviours;
using KoA.Core.Models;
using KoA.Core.Util;
using TaleWorlds.Core;
using TaleWorlds.Library;
using TaleWorlds.ModuleManager;
using TaleWorlds.MountAndBlade;

namespace KoA.Core
{
    public class SubModule : MBSubModuleBase
    {
        private bool _isInitialized;
        private List<ModuleInfo> _loadedModules;
        private ModuleInfo _module;

        private WeaponOverhaul _weaponOverhaul;

        protected override void OnSubModuleLoad()
        {
            _weaponOverhaul = new WeaponOverhaul();

            AppDomain.CurrentDomain.FirstChanceException += DomainExceptionHandling.CurrentDomainOnFirstChanceException;
            AppDomain.CurrentDomain.UnhandledException += DomainExceptionHandling.CurrentDomainUnhandledException;

            new Harmony("KoACorePatches").PatchAll();
        }


        protected override void OnBeforeInitialModuleScreenSetAsRoot()
        {
            if (_isInitialized) return;

            _loadedModules = ModuleHelper.GetModules().ToList();
            _module = _loadedModules.FirstOrDefault(m => m.Id.ToLower() == "kingdomsofarda");

            // Bannerlord appends the game build onto the version string from the module
            var versionString = _module?.Version.ToString();

            InformationManager.DisplayMessage(new InformationMessage(
                $"Loaded Kingdoms of Arda [ Version {versionString?.Substring(0, 6) ?? "Error"} | Build {versionString?.Substring(7) ?? "Error"} ]",
                new Color(0.05f, 1f, 0.05f)));

            _isInitialized = true;
        }

        public override void OnMissionBehaviorInitialize(Mission mission)
        {
            /*
             * The below MissionBehaviours will be added to ALL Missions
             * In the future, it may be wise to create our own Missions.
             * See SandBox.SandBoxMissions and ICampaignMissionManager for more information on how to do this.
             */
            mission.AddMissionBehavior(new ArmourBehavior());
        }

        public override void OnGameInitializationFinished(Game game)
        {
            _weaponOverhaul.Load();
            _weaponOverhaul.UpdateWeapons();
        }
        
        protected override void OnGameStart(Game game, IGameStarter gameStarterObject)
        {
            RemoveGameModels(gameStarterObject);
        }
        
        private void RemoveGameModels(IGameStarter gameStarterObject)
        {
            var gamemodels = (List<GameModel>)gameStarterObject.Models;

            gamemodels.RemoveAll(model => model.GetType() == typeof(DefaultDamageParticleModel));
            gamemodels.Add(new KoADamageParticleModel());
        }
    }
}