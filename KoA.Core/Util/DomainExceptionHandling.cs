using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Windows.Forms;
using HarmonyLib;

namespace KoA.Core.Util
{
    public static class DomainExceptionHandling
    {
        private static readonly Queue<Error> ErrorQueue = new();

        private static string PathToLog { get; } =
            AppDomain.CurrentDomain.BaseDirectory + "..\\..\\Modules\\KingdomsOfArda\\CrashLog.log";


        public static void CurrentDomainOnFirstChanceException(object sender, FirstChanceExceptionEventArgs e)
        {
            if (ErrorQueue.Count == 3) ErrorQueue.Dequeue();

            var error = new Error
            {
                Message = e.Exception.Message,
                StackTrace = new StackTrace(true).ToString(),
                FullDescription = e.Exception.TargetSite.FullDescription(),
                Type = e.Exception.GetType().ToString()
            };

            ErrorQueue.Enqueue(error);
        }

        public static void CurrentDomainUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            var stringBuilder = new StringBuilder();

            while (ErrorQueue.Count > 0)
            {
                var error = ErrorQueue.Dequeue();

                MessageBox.Show(error.FullDescription + ": " + error.Message,
                    error.Type);

                stringBuilder.Append("==================Message=================\n" +
                                     error.Message +
                                     "\n\n==================StackTrace=================\n" +
                                     error.StackTrace);
            }


            File.WriteAllText(PathToLog, stringBuilder.ToString());
        }

        private struct Error
        {
            public string StackTrace;
            public string FullDescription;
            public string Message;
            public string Type;
        }
    }
}