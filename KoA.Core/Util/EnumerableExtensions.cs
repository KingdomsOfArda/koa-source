using System;
using System.Collections.Generic;
using System.Linq;

namespace KoA.Core.Util
{
    public static class EnumerableExtensions
    {
        /// <summary>
        ///     Replaces the given value with the given Type
        /// </summary>
        /// <param name="enumerable"></param>
        /// <param name="findType">To be removed type</param>
        /// <param name="toReplace">To be replaced</param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> Replace<T>(this IEnumerable<T> enumerable, Type findType, T toReplace)
        {
            return enumerable.Select(item => item.GetType() == findType ? toReplace : item);
        }
    }
}