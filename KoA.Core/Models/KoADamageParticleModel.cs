﻿using TaleWorlds.Engine;
using TaleWorlds.MountAndBlade;

namespace KoA.Core.Models
{
    public class KoADamageParticleModel : DamageParticleModel
    {
        private readonly int _bloodContinueHitParticleIndex = -1;
        private readonly int _bloodEndHitParticleIndex = -1;
        private readonly int _bloodStartHitParticleIndex = -1;
        private readonly int _missileHitParticleIndex = -1;
        private readonly int _sweatContinueHitParticleIndex = -1;
        private readonly int _sweatEndHitParticleIndex = -1;
        private readonly int _sweatStartHitParticleIndex = -1;
        private readonly int _urukbloodContinueHitParticleIndex = -1;
        private readonly int _urukbloodEndHitParticleIndex = -1;
        private readonly int _urukbloodStartHitParticleIndex = -1;
        private readonly int _urukmissileHitParticleIndex = -1;
        private readonly int _uruksweatContinueHitParticleIndex = -1;
        private readonly int _uruksweatEndHitParticleIndex = -1;
        private readonly int _uruksweatStartHitParticleIndex = -1;

        public KoADamageParticleModel()
        {
            // Default BL particles
            _bloodStartHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("psys_game_blood_sword_enter");
            _bloodContinueHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("psys_game_blood_sword_inside");
            _bloodEndHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("psys_game_blood_sword_exit");
            _sweatStartHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("psys_game_sweat_sword_enter");
            _sweatContinueHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("psys_game_sweat_sword_enter");
            _sweatEndHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("psys_game_sweat_sword_enter");
            _missileHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("psys_game_blood_sword_enter");
            
            // Uruk particles
            _urukbloodStartHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("koa_blood_sword_enter_uruk");
            _urukbloodContinueHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("koa_blood_sword_inside_uruk");
            _urukbloodEndHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("koa_blood_sword_exit_uruk");
            _uruksweatStartHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("psys_game_sweat_sword_enter");
            _uruksweatContinueHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("psys_game_sweat_sword_enter");
            _uruksweatEndHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("psys_game_sweat_sword_enter");
            _urukmissileHitParticleIndex = ParticleSystemManager.GetRuntimeIdByName("koa_blood_sword_exit_uruk");
        }

        public override void GetMeleeAttackBloodParticles(
            Agent attacker,
            Agent victim,
            in Blow blow,
            in AttackCollisionData collisionData,
            out HitParticleResultData particleResultData)
        {
            if (victim.IsFemale)
            {
                // Uruk blood effects
                particleResultData.StartHitParticleIndex = _urukbloodStartHitParticleIndex;
                particleResultData.ContinueHitParticleIndex = _urukbloodContinueHitParticleIndex;
                particleResultData.EndHitParticleIndex = _urukbloodEndHitParticleIndex;
            }
            else
            {
                // Human blood effects
                particleResultData.StartHitParticleIndex = _bloodStartHitParticleIndex;
                particleResultData.ContinueHitParticleIndex = _bloodContinueHitParticleIndex;
                particleResultData.EndHitParticleIndex = _bloodEndHitParticleIndex;
            }
        }

        public override void GetMeleeAttackSweatParticles(
            Agent attacker,
            Agent victim,
            in Blow blow,
            in AttackCollisionData collisionData,
            out HitParticleResultData particleResultData)
        {
            if (victim.IsFemale)
            {
                // Uruk sweat effects
                particleResultData.StartHitParticleIndex = _uruksweatEndHitParticleIndex;
                particleResultData.ContinueHitParticleIndex = _uruksweatContinueHitParticleIndex;
                particleResultData.EndHitParticleIndex = _uruksweatEndHitParticleIndex;
            }
            else
            {
                // Human sweat effects
                particleResultData.StartHitParticleIndex = _sweatStartHitParticleIndex;
                particleResultData.ContinueHitParticleIndex = _sweatContinueHitParticleIndex;
                particleResultData.EndHitParticleIndex = _sweatEndHitParticleIndex;
            }
        }

        public override int GetMissileAttackParticle(
            Agent attacker,
            Agent victim,
            in Blow blow,
            in AttackCollisionData collisionData)
        {
            if (victim.IsFemale)
            {
                // Uruk sweat effects
                return _urukmissileHitParticleIndex;
            }
            else
            {
                // Human sweat effects
                return _missileHitParticleIndex;
            }
        }
    }
}