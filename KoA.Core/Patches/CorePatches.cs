﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using HarmonyLib;
using TaleWorlds.Library;
using TaleWorlds.ModuleManager;
using TaleWorlds.MountAndBlade;
using TaleWorlds.ObjectSystem;
using StringWriter = System.IO.StringWriter;

namespace KoA.Core.Patches
{
    [HarmonyPatch]
    public static class CorePatches
    {
        [HarmonyPrefix]
        [HarmonyPatch(typeof(MBObjectManager), "GetMergedXmlForManaged")]
        public static bool SkipXMLValidationPatch(string id,
            ref bool skipValidation,
            bool ignoreGameTypeInclusionCheck = true,
            string gameType = "")
        {
            skipValidation = true;
            if(id == "SiegeEngines")
                skipValidation = false;
            return true;
        }

        [HarmonyPrefix]
        [HarmonyPatch(typeof(Module), "CreateProcessedSkinsXMLForNative")]
        public static bool Prefix(ref string __result, ref string baseSkinsXmlPath)
        {
            Debug.Print("Prioritizing KoA skins.xml");
            var source = new List<string>();
            var tupleList = new List<Tuple<string, string>>();
            var stringList = new List<string>();
            var list = XmlResource.MbprojXmls
                .Where(
                    x => x.Id == "soln_skins")
                .ToList();
            list.Reverse();
            foreach (var objectXmlInformation in list)
            {
                if (File.Exists(ModuleHelper.GetXmlPathForNative(objectXmlInformation.ModuleName,
                    objectXmlInformation.Name)))
                {
                    source.Add(ModuleHelper.GetXmlPathForNativeWBase(objectXmlInformation.ModuleName,
                        objectXmlInformation.Name));
                    tupleList.Add(Tuple.Create(
                        ModuleHelper.GetXmlPathForNative(objectXmlInformation.ModuleName,
                            objectXmlInformation.Name), string.Empty));
                }

                var xsltPathForNative =
                    ModuleHelper.GetXsltPathForNative(objectXmlInformation.ModuleName,
                        objectXmlInformation.Name);
                var path = xsltPathForNative + "t";
                stringList.Add(File.Exists(xsltPathForNative)
                    ? xsltPathForNative
                    : File.Exists(path)
                        ? path
                        : string.Empty);
            }

            var stringWriter = new StringWriter();
            var xmlTextWriter = new XmlTextWriter(stringWriter);
            var mergedXmlFile = MBObjectManager.CreateMergedXmlFile(tupleList, stringList, true);
            mergedXmlFile.WriteTo(xmlTextWriter);
            baseSkinsXmlPath = source.Find(x => x.Contains("KingdomsOfArda"));
            __result = stringWriter.ToString();
            return false;
        }
    }
}