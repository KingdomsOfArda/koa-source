﻿using System;
using System.Linq;
using System.Reflection;
using HarmonyLib;
using TaleWorlds.MountAndBlade;
using TaleWorlds.MountAndBlade.View.Missions;

namespace KoA.SoundSystem
{
    public class SubModule : MBSubModuleBase
    {
        public static Harmony Patcher = new("KoA_SoundSystem");

        protected override void OnSubModuleLoad()
        {
            Patcher.PatchAll();

            // Set Current Property to prevent NullReferenceExceptions
            typeof(MBMusicManager)?.GetProperty("Current", BindingFlags.Static | BindingFlags.Public)
                ?.SetValue(null, Activator.CreateInstance(typeof(MBMusicManager), true));
        }

        public override void OnMissionBehaviorInitialize(Mission mission)
        {
            // Remove MissionBehaviours which use MBMusicManager
            var musicBattleMV = mission.MissionBehaviors.FirstOrDefault(m => m is MusicBattleMissionView);
            if (musicBattleMV != null)
                mission.RemoveMissionBehavior(musicBattleMV);

            var musicTournamentMV = mission.MissionBehaviors.FirstOrDefault(m => m is MusicTournamentMissionView);
            if (musicTournamentMV != null)
                mission.RemoveMissionBehavior(musicTournamentMV);

            var musicSilencedMV = mission.MissionBehaviors.FirstOrDefault(m => m is MusicSilencedMissionView);
            if (musicSilencedMV != null)
                mission.RemoveMissionBehavior(musicSilencedMV);
        }

        protected override void OnSubModuleUnloaded()
        {
        }
    }
}