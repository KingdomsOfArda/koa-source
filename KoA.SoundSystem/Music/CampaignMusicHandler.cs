﻿using KoA.Core.Culture;
using TaleWorlds.CampaignSystem;
using TaleWorlds.MountAndBlade;

namespace KoA.SoundSystem.Music
{
    public class CampaignMusicHandler : IMusicHandler
    {
        public bool IsPausable => false;

        public void OnUpdated(float dt)
        {
            TickCampaignMusic(dt);
        }

        public static void Create()
        {
            var handler = new CampaignMusicHandler();
            // pass to music manager
        }

        private void TickCampaignMusic(float dt)
        {
            // Add music logic here
        }

        private KoACultureCode GetNearbyCulture()
        {
            CultureObject cultureObject = null;
            var closest = float.MaxValue;
            foreach (var settlement in Campaign.Current.Settlements)
            {
                if (!settlement.IsTown && !settlement.IsVillage) continue;
                var distanceSquared = settlement.Position2D.DistanceSquared(PartyBase.MainParty.Position2D);

                if (settlement.IsVillage) distanceSquared *= 1.05f;

                if (!(closest > distanceSquared)) continue;
                cultureObject = settlement.Culture;
                closest = distanceSquared;
            }

            return cultureObject.GetKoACultureCode();
        }
    }
}