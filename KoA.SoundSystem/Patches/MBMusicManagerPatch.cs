﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HarmonyLib;
using TaleWorlds.MountAndBlade;

namespace KoA.SoundSystem.Patches
{
    /// <summary>
    ///     Patches to dereference MBMusicManager in Bannerlord so that we can replace it with out own system.
    /// </summary>
    [HarmonyPatch]
    public class MBMusicManagerPatch
    {
        private static IEnumerable<MethodBase> TargetMethods()
        {
            return typeof(MBMusicManager)
                .GetMethods()
                .Where(method => method.ReturnType == typeof(void));
        }

        private static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);
            codes.RemoveRange(0, codes.Count - 1);
            return codes.AsEnumerable();
        }
    }
}